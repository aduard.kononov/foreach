from foreach import foreach, SharedDictDescriptor, SharedDict
import os
import requests
import urllib.parse
import threading
import tempfile

from lxml import html
from lxml.html import HtmlElement


def get_google_logo_url():
    site_url = 'https://google.com/'
    response = requests.get(site_url)
    root: HtmlElement = html.fromstring(response.content)
    img_src = str(root.xpath('string(//img[@alt="Google"]/@src)'))
    img_url = urllib.parse.urljoin(site_url, img_src)
    return img_url


def worker(img_url, shared_dict: SharedDict):
    thread_id = threading.current_thread().ident
    print(f'{thread_id}: downloading image')
    response = requests.get(img_url)
    raw_image = response.raw.read()
    print(f'{thread_id}: downloaded image')

    print(f'{thread_id}: saving the image')
    descr, temp_file_path = tempfile.mkstemp()
    with open(temp_file_path, 'wb') as file:
        file.write(raw_image)
    print(f'{thread_id}: saved the image')
    shared_dict['counter'] += 1
    return descr


def main():
    google_logo_url = get_google_logo_url()
    urls = (google_logo_url for _ in range(15))

    sharable_data = {
        'counter': 0,
    }
    shared_dict_descriptor = SharedDictDescriptor(sharable_data)
    results = foreach(worker, urls, shared_dict_descriptor, 'process')
    print(results)


if __name__ == '__main__':
    main()
