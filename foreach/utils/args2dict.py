import re
from foreach.utils.get_calling_code import get_invocation_way

__all__ = ['args2dict']


def _parse_var_names(calling_code: str):
    calling_code = re.sub(r'(?:.*?\(|\))', '', calling_code)
    var_names = [
        var_name
        for var_name in calling_code.split(',')
        if var_name
    ]
    return var_names


def args2dict(*args):
    calling_code = get_invocation_way()
    var_names = _parse_var_names(calling_code)

    is_variable_name = lambda x: re.fullmatch(r'[A-Za-z_]\w*', x)
    for name in var_names:
        if not is_variable_name(name):
            raise Exception(
                f'\"{name}\" is not a valid variable name\n'
                f'Calling code: {calling_code}'
            )

    dyn_dict = {
        var_name: var_value
        for var_name, var_value in zip(var_names, args)
    }
    return dyn_dict


def main():
    x, y, z = 1, 2, 3
    d = args2dict(x, y, z)
    print(d)


if __name__ == '__main__':
    main()
