import inspect
import io
import re


class StrLiteralsSaver:
    def __init__(self):
        self.data = {}

    def save(self, text: str):
        literals_regex = re.compile(r'(?:\'[\w\W]*?\'|\"[\w\W]*?\")')
        for i, match in enumerate(literals_regex.finditer(text)):
            mark = f'%<<{i}_STR_LITERAL>>%'
            match_text = match.group(0)
            text = text.replace(match_text, mark, 1)
            self.data[mark] = match_text
        return text

    def restore(self, text: str):
        keys_regex = re.compile(r'|'.join(self.data.keys()))
        for match in keys_regex.finditer(text):
            match_text = match.group(0)
            replace_with = self.data[match_text]
            text = text.replace(match_text, replace_with, 1)
        return text


def get_invocation_way() -> str:
    cur_frame = inspect.currentframe()
    outer_frames = inspect.getouterframes(cur_frame)

    frame_called_from_info = outer_frames[2]
    with open(frame_called_from_info.filename) as file:
        src = file.read()

    saver = StrLiteralsSaver()
    src = saver.save(src)

    src = io.StringIO(src)

    call_lineno = frame_called_from_info.lineno

    calling_code = ''
    for index, line in enumerate(src, 1):
        if index >= call_lineno:
            calling_code += line
            if ')' in calling_code:
                break

    cleared_calling_code = re.sub(r'\s+', '', calling_code)

    frame_called_from_info = outer_frames[1].function
    pattern = re.compile(r'{}\(.*?\)'.format(frame_called_from_info))
    res = pattern.search(cleared_calling_code).group(0)
    res = saver.restore(res)
    return res
