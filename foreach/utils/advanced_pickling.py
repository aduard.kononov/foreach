import dill

from typing import Callable, Iterable
from functools import partial

__all__ = ['map_pickling']


# an absence of * and ** is NOT a mistake
def _unpickle_and_call(dumped_function, item, args, kwargs):
    worker_func = dill.loads(dumped_function)
    res = worker_func(item, *args, **kwargs)
    return res


def map_pickling(worker_func: Callable, items: Iterable, *args, **kwargs):
    """
        >>> from multiprocessing import Pool
        >>> images = [...]
        >>> with Pool() as pool:
        ...     worker, items = map_pickling(
        ...         process_image,
        ...         images,
        ...         type='png'
        ...         **options,
        ...     )
        ...     results = pool.map(worker, items)
        >>>
    """
    dumped_worker_func = dill.dumps(worker_func)
    partial_unpickle = partial(
        _unpickle_and_call,
        dumped_worker_func,
        args=args,
        kwargs=kwargs
    )
    return partial_unpickle, items
