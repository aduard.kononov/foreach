from multiprocessing import Pool as ProcessPool, Manager as ProcessManager
from multiprocessing.dummy import Pool as ThreadPool, Manager as ThreadManager
from typing import NewType, Union, Callable, ContextManager, Protocol
from abc import abstractmethod

AnyPool = NewType('AnyPool', Union[ThreadPool, ProcessPool])
AnyManager = NewType('AnyManager', Union[ThreadManager, ProcessManager])
ContextManagerBuilder = NewType('ContextManagerBuilder', Callable[[], ContextManager])


class LockLike(Protocol):
    @abstractmethod
    def acquire(self):
        pass

    @abstractmethod
    def release(self):
        pass
