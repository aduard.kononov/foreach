import contextlib
import inspect

__VANILLA_PRINT = print

__all__ = ['safe_print']


@contextlib.contextmanager
def safe_print(locker):
    def new_print(*args, **kwargs):
        with locker:
            print(*args, **kwargs)

    inspect.builtins.print = new_print
    yield
    inspect.builtins.print = __VANILLA_PRINT
