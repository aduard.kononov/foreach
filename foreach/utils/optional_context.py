from foreach.utils.additional_types import ContextManagerBuilder

from contextlib import contextmanager

__all__ = ['optional_context']


@contextmanager
def optional_context(
        use: bool,
        context_manager_builder: ContextManagerBuilder,
        *args, **kwargs
):
    if use:
        with context_manager_builder(*args, **kwargs) as manager:
            yield manager
    else:
        yield None


def main():
    @contextmanager
    def ready_steady_go():
        print('ready')
        yield 'steady'
        print('go')

    with optional_context(use=True, context_manager_builder=ready_steady_go) as context:
        print(context)

    with optional_context(use=False, context_manager_builder=ready_steady_go) as context:
        print(context)


if __name__ == '__main__':
    main()
