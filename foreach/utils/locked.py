from .additional_types import LockLike

from contextlib import contextmanager

__all__ = ['locked']


@contextmanager
def locked(lock: LockLike):
    """
        >>> def worker(nums, shared_dict: dict,nums=None):
        ...     lock = shared_dict['lock']
        ...     with locked(lock):
        ...         shared_dict['called'] += 1
        ...     return [n**2 for n in nums]
    """
    lock.acquire()
    try:
        yield
    finally:
        lock.release()
