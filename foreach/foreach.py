from .utils import map_pickling, optional_context, safe_print
from .shared_dict import SharedDictDescriptor, shared_dict_context, SharedDict

from multiprocessing import Pool as ProcessPool, Manager as ProcessManager
from multiprocessing.dummy import Pool as ThreadPool, Manager as ThreadManager

from typing import Optional, Callable, Iterable, List, Any

__all__ = ['foreach']


# TODO(ekon): build in tqdm
# TODO(ekon): make it possible to choose between map, imap, imap_unordered etc.
# FIXME(ekon): (imap) if chuncksize is bigger than len(iterator), the function does nothing
def foreach(
        worker_func: Callable,
        items: Iterable,
        shared_dict_descriptor: Optional[SharedDictDescriptor],
        spawn: str  # 'thread' or 'process'
) -> List[Any]:
    if spawn == 'thread':
        pool_builder = ThreadPool
        manager_builder = ThreadManager
        raise NotImplementedError("There is a problem with ThreadManager: it seems to behave differently"
                                  "from ProcessManager")
    elif spawn == 'process':
        pool_builder = ProcessPool
        manager_builder = ProcessManager
    else:
        raise Exception(f"Expected 'thread' or 'process', got {spawn}")

    # If swap these context managers, you will probably face with a file not found error. Read this:
    # https://stackoverflow.com/questions/56641428/python-3-6-nested-multiprocessing-managers-cause-filenotfounderror

    with shared_dict_context(manager_builder, shared_dict_descriptor) as shared_dict:
        shared_dict: SharedDict
        with pool_builder() as pool:
            special_map_pickling: tuple = map_pickling(
                worker_func=worker_func,
                items=items,
                shared_dict=shared_dict,
            )

            optional_safe_print = lambda: optional_context(bool(shared_dict), safe_print, shared_dict.locker)
            with optional_safe_print():
                results = pool.map(*special_map_pickling)

            # If we do not get rid of the generator here, within the pool context,
            # we won't be able to do it anywhere else
            results = results if type(results) is list else list(results)
    return results
