from .utils import locked, optional_context
from .utils.additional_types import AnyManager, LockLike

import functools
import contextlib

from typing import Union, Optional
from dataclasses import dataclass


@dataclass
class SharedDictDescriptor:
    sharable: dict
    need_locker: bool = True
    _lock: LockLike = None

    def build(self, manager: AnyManager):
        sd = manager.dict()
        if self.need_locker:
            self._lock = manager.Lock()
            sd['locker'] = functools.partial(locked, self._lock)
        sd['data'] = self.sharable
        return sd


class SharedDict:
    def __init__(self, manager: AnyManager, descriptor: SharedDictDescriptor):
        self._shared_dict = descriptor.build(manager)

    def __getitem__(self, key):
        print(f'{type(self.locker)}: {dir(self.locker)}')
        with self.locker:
            return self._data[key]

    def __setitem__(self, key, value):
        if 'locker' not in self._shared_dict:
            raise KeyError('Your SharedDict does not use locker')
        with self.locker:
            self._data[key] = value

    @property
    def locker(self) -> LockLike:
        return self._shared_dict['locker']

    @property
    def _data(self):
        return self._shared_dict['data']


@contextlib.contextmanager
def shared_dict_context(manager_builder: AnyManager, descriptor: SharedDictDescriptor) -> Optional[SharedDict]:
    optional_manager = lambda: optional_context(bool(descriptor), manager_builder)
    with optional_manager() as manager:
        if manager:
            yield SharedDict(manager, descriptor)
        else:
            yield None
